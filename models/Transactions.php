<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property integer $sourceuid
 * @property integer $destuid
 * @property double $sum
 * @property string $datetime
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sourceuid', 'destuid', 'sum'], 'required'],
            [['sourceuid', 'destuid'], 'integer'],
            [['sum'], 'number'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sourceuid' => Yii::t('app', 'Sourceuid'),
            'destuid' => Yii::t('app', 'Destuid'),
            'sum' => Yii::t('app', 'Sum'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }

    /**
     * @inheritdoc
     * @return TransactionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransactionsQuery(get_called_class());
    }
}
