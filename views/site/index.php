<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Тестовое задание</h1>

        <p class="lead">Перевод средств пользователей со счета на счет  </p>
        
       
    </div>

    <div class="body-content">

        <div class="row"> <div class="col-lg-4">
                <h2 style="text-align: center">Выбор источника</h2>

                <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered',
            'id' => 'sourtable'
        ],
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
        return ['id' => $model['id'], 'onclick' => 'setrowsource(this.id);'];
        },
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],
         //   ['class' => 'yii\grid\CheckboxColumn',

//             'checkboxOptions' => function($model, $key, $index, $column) {
//                         global $estimaters;
//                 $bool = in_array($model->idestimaters, $estimaters);
//                 return ['checked' => $bool];
//             }
         //       ],
            'id',
            'name',
            'balance',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
            </div> 
             
            
          
            <div class="col-lg-4">
                   <h2 style="text-align: center">Транзакция</h2>
              <div class="site-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($tmodel, 'sourceuid')->hiddenInput()->label('') ?>
        <?= $form->field($tmodel, 'destuid')->hiddenInput()->label('') ?>
        <?= $form->field($tmodel, 'sum') ?>
      
    
                  <div class="form-group" style="text-align: center">
        
          <?= Html::button('Перевести средства',
        ['type'=>'button', 'class'=>'btn btn-primary', 'id' => 'btn-submit']);    
        ?> 
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-form -->

                
            </div>
         <div class="col-lg-4">
                <h2 style="text-align: center">Выбор приемника</h2>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
    'tableOptions' => [
            'class' => 'table table-striped table-bordered',
            'id' => 'dest'
        ],
        'filterModel' => $searchModel,
    'rowOptions' => function ($model, $key, $index, $grid) {
        return ['id' => $model['id'], 'onclick' => 'setrowdestinate(this.id);'];
        },
        'columns' => [
        //    ['class' => 'yii\grid\SerialColumn'],
         // ['class' => 'yii\grid\CheckboxColumn',

//             'checkboxOptions' => function($model, $key, $index, $column) {
//                         global $estimaters;
//                 $bool = in_array($model->idestimaters, $estimaters);
//                 return ['checked' => $bool];
//             }
    //            ],
            'id',
            'name',
            'balance',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
               
            </div>
        </div>

    </div>
</div>
   